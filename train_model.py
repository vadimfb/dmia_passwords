import argparse
import yaml
import pandas as pd

from model_pipeline.model import PasswordModel
from model_pipeline.feature_utils import load_words, get_keyboard_ngrams

parser = argparse.ArgumentParser()
parser.add_argument('--config_path')

if __name__ == '__main__':

    args = parser.parse_args()
    with open(args.config_path, 'r') as f:
        config = yaml.safe_load(f)

    train_df = pd.read_csv(config['train_data_csv'], engine='python')
    train_df.dropna(inplace=True)
    train_df.reset_index(drop=True, inplace=True)

    model_parameters = config.get('model_parameters', dict())
    path_to_words = config['words']
    valid_words = load_words(path_to_words)
    keyboard_ngrams = get_keyboard_ngrams()
    model_parameters['valid_words'] = valid_words
    model_parameters['keyboard_ngrams'] = keyboard_ngrams
    model = PasswordModel(**model_parameters)
    X = train_df[['Password']]
    y = train_df['Times']
    model.fit(X, y)
    model.save(dir_name=config['model_path'])


