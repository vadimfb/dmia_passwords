from catboost import CatBoost
from catboost import Pool
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split
from typing import Tuple, Set

import pandas as pd
import numpy as np
import joblib
import os
import json

from .feature_utils import calculate_features


class PasswordModel:
    """
    Password Model Class for train/predict
    Related competition: https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords

    Parameters
    ----------
    max_features: int, default=10000
        max_features in TfidfVectorizer from sklearn
    ngram_range: Tuple[int, int], default=(1,4)
        ngram_range in TfidfVectorizer from sklearn
    lowercase: bool, default=True
        lowercase in TfidfVectorizer from sklearn
    analyzer: str, default=0
        analyzer in TfidfVectorizer from sklearn
    alpha: float, default=0.1
        alpha in Ridge from sklearn
    random_state: int, default=0
        random state for everything
    keyboard_ngrams: Set[str], default=None
        keyboard ngrams for calculate number of keyboard ngrams in Password
    valid_words: Set[str], default=None
        english words for calculate number of words in Password
    iterations: int, default=3000
        iterations in CatBoost
    learning_rate: float, default=0.01
        learning_rate in CatBoost
    depth: int, default=5
        depth in CatBoost
    loss_function: str, default='RMSE'
        loss_function in CatBoost
    custom_metric: str, default='RMSE'
        custom_metric in CatBoost
    """

    def __init__(self,
                 max_features: int = 10000,
                 ngram_range: Tuple[int, int] = (1, 4),
                 lowercase: bool = True,
                 analyzer: str = 'char',
                 alpha: float = 0.1,
                 random_state: int = 0,
                 keyboard_ngrams: Set[str] = None,
                 valid_words: Set[str] = None,
                 iterations: int = 3000,
                 learning_rate: float = 0.01,
                 depth: int = 5,
                 loss_function: str = 'RMSE',
                 custom_metric: str = 'RMSE'):

        # set parameters
        self.max_features = max_features
        self.ngram_range = ngram_range
        self.lowercase = lowercase
        self.analyzer = analyzer
        self.alpha = alpha
        self.random_state = random_state
        self.iterations = iterations
        self.depth = depth
        self.learning_rate = learning_rate
        self.loss_function = loss_function
        self.custom_metric = custom_metric
        self.keyboard_ngrams = keyboard_ngrams
        self.valid_words = valid_words
        if keyboard_ngrams is None:
            self.keyboard_ngrams = set()
        if valid_words is None:
            self.valid_words = set()

        # init attributes
        self.feature_names = []
        self.vectorizer = None
        self.ridge_estimator = None
        self.catboost_estimator = None

    def fit(self, X: pd.DataFrame, y: pd.Series):
        """
        Fit method
        :param X: pd.DataFrame
            dataframe with one column 'Password'
        :param y: pd.Series
            series with Times
        :return: None
        """

        # transform target
        y = np.log1p(y.values)
        # init Tfidf vectorizer
        vectorizer = TfidfVectorizer(analyzer=self.analyzer,
                                     lowercase=self.lowercase,
                                     max_features=self.max_features,
                                     ngram_range=self.ngram_range)

        # init ridge model
        ridge_model = Ridge(alpha=self.alpha, random_state=self.random_state)

        # split train dataframe for independent learning ridge and catboost
        X1, X2, y1, y2 = train_test_split(X, y, test_size=0.5, shuffle=True, random_state=self.random_state)
        X1.reset_index(drop=True, inplace=True)
        X2.reset_index(drop=True, inplace=True)

        # fit vectorizer on whole dataset
        vectorizer.fit(X['Password'].astype(str))

        # fit ridge on first part
        sparse_features = vectorizer.transform(X1['Password'].astype(str))
        ridge_model.fit(X=sparse_features, y=y1)

        # predict ridge for catboost
        sparse_features = vectorizer.transform(X2['Password'].astype(str))
        predictions = ridge_model.predict(sparse_features)
        X2['prediction'] = predictions
        # calculate features for catboost
        self.feature_names.append('prediction')
        X2, feature_names = calculate_features(X2, self.keyboard_ngrams, self.valid_words)
        self.feature_names.extend(feature_names)

        # create Pool for train
        train_pool = Pool(
            data=X2[self.feature_names],
            label=y2
        )

        # init and fir catboost
        catboost_model = CatBoost({
            'iterations': self.iterations,
            'learning_rate': self.learning_rate,
            'depth': self.depth,
            'loss_function': self.loss_function,
            'custom_metric': self.custom_metric,
            'random_state': self.random_state
        })
        catboost_model.fit(train_pool, verbose=False)

        # set attributes
        self.vectorizer = vectorizer
        self.ridge_estimator = ridge_model
        self.catboost_estimator = catboost_model

    def predict(self, X: pd.DataFrame) -> np.ndarray:
        """
        Predict method

        :param X: pd.DataFrame
            dataframe with one column 'Password'
        :return: np.ndarray
            array with predictions
        """

        # feature preprocessing
        X['Password'] = X['Password'].astype(str)
        sparse_features = self.vectorizer.transform(X['Password'])
        ridge_preds = self.ridge_estimator.predict(sparse_features)
        X['prediction'] = ridge_preds
        X = calculate_features(X, self.keyboard_ngrams, self.valid_words, 'test')
        pool = Pool(
            data=X[self.feature_names],
        )
        # catboost predict
        result_preds = self.catboost_estimator.predict(pool)
        # inverse transform
        result_preds = np.expm1(result_preds)
        return result_preds

    def save(self, dir_name: str = 'data/model'):
        """
        save method

        :param dir_name: str
            dir to save model
        :return: None
        """

        joblib.dump(self.vectorizer, os.path.join(dir_name, 'vectorizer.pkl'))
        joblib.dump(self.ridge_estimator, os.path.join(dir_name, 'ridge.pkl'))
        self.catboost_estimator.save_model(fname=os.path.join(dir_name, 'catboost.cbm'), format='cbm')

        with open(os.path.join(dir_name, 'feature_names.json'), 'w') as f:
            json.dump(self.feature_names, f)

        with open(os.path.join(dir_name, 'parameters.json'), 'w') as f:
            parameters = dict()
            parameters['max_features'] = self.max_features
            parameters['ngram_range'] = self.ngram_range
            parameters['lowercase'] = self.lowercase
            parameters['analyzer'] = self.analyzer
            parameters['alpha'] = self.alpha
            parameters['random_state'] = self.random_state
            parameters['iterations'] = self.iterations
            parameters['depth'] = self.depth
            parameters['learning_rate'] = self.learning_rate
            parameters['loss_function'] = self.loss_function
            parameters['custom_metric'] = self.custom_metric
            parameters['keyboard_ngrams'] = list(self.keyboard_ngrams)
            parameters['valid_words'] = list(self.valid_words)
            json.dump(parameters, f)

    @classmethod
    def load(cls, dir_name: str = 'data/model'):
        """
        Load method

        :param dir_name: str
            dir to load model
        :return: PasswordModel
            loaded PasswordModel
        """

        vectorizer = joblib.load(os.path.join(dir_name, 'vectorizer.pkl'))
        ridge_estimator = joblib.load(os.path.join(dir_name, 'ridge.pkl'))
        catboost_estimator = CatBoost().load_model(fname=os.path.join(dir_name, 'catboost.cbm'), format='cbm')

        with open(os.path.join(dir_name, 'feature_names.json'), 'r') as f:
            feature_names = json.load(f)

        with open(os.path.join(dir_name, 'parameters.json'), 'r') as f:
            parameters = json.load(f)

        parameters['keyboard_ngrams'] = set(parameters['keyboard_ngrams'])
        parameters['valid_words'] = set(parameters['valid_words'])

        password_model = cls(**parameters)
        password_model.vectorizer = vectorizer
        password_model.ridge_estimator = ridge_estimator
        password_model.catboost_estimator = catboost_estimator
        password_model.feature_names = feature_names

        return password_model
