import pandas as pd
from typing import List, Set, Union, Tuple


def get_char_ngrams(string: str, nrange: Tuple[int, int] = (2, 5)) -> Set[str]:
    """
    Method to get char ngrams of string
    :param string: str
    :param nrange: Tuple[int, int]
    :return: Set[str]
        set of ngrams
    """
    ngrams = set()
    for i in range(len(string)):
        for j in range(nrange[0], nrange[1] + 1):
            ngrams.add(string[i: i + j])

    return ngrams


def get_keyboard_ngrams() -> Set[str]:
    """
    Method to get keyboard char ngrams of string
    :return: Set[str]
    """
    keyboard1 = 'qwertyuiopasdfghjklzxcvbnm'
    keyboard2 = 'qazwsxedcrfvtgbyhnujmikolp'
    keyboard3 = '1234567890'
    keyboard4 = '0123456789'
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.lower()

    sequences = [keyboard1, keyboard2, keyboard3, keyboard4, alphabet]

    keyboard_ngrams = set()
    for sequence in sequences:
        keyboard_ngrams.update(get_char_ngrams(sequence))
        keyboard_ngrams.update(get_char_ngrams(sequence[::-1]))

    return keyboard_ngrams


def load_words(path: str) -> Set[str]:
    """
    Method to load english words
    :param path: str
    :return: Set[str]
        set of english words
    """

    with open(path) as word_file:
        valid_words = set(word_file.read().split())

    return valid_words


def calculate_features(df: pd.DataFrame,
                       keyboard_ngrams: Set[str],
                       valid_words: Set[str],
                       mode: str = 'train') -> Union[pd.DataFrame, Tuple[pd.DataFrame, List[str]]]:
    """
    Method to calculate features for catboost

    :param df: pd.DataFrame
    :param keyboard_ngrams: Set[str]
    :param valid_words: Set[str]
    :param mode: str, default='train'
    :return: Union[pd.DataFrame, Tuple[pd.DataFrame, List[str]]]
        pd.DataFrame if mode != 'train', else Tuple[pd.DataFrame, List[str]]
    """

    _df = df.copy(deep=True)
    passwords = _df.Password.astype(str).str
    _df['len'] = passwords.len()
    _df['has_digit'] = passwords.contains(r'\d+')
    _df['has_alpha'] = passwords.contains(r'\w+')
    _df['is_digit'] = passwords.isdigit()
    _df['is_alpha'] = passwords.isalpha()
    _df['has_alpha_and_digit'] = _df['has_digit'] & _df['has_alpha']
    _df['has_lower'] = passwords.contains(r'[a-z]+')
    _df['has_upper'] = passwords.contains(r'[A-Z]+')
    _df['has_lower_and_upper'] = _df['has_lower'] & _df['has_upper']
    _df['is_alphanumeric'] = passwords.isalnum()

    _df['password_ngrams'] = passwords.lower().astype(str).apply(lambda x: get_char_ngrams(x, (2, 10)))
    _df['has_keyboard'] = _df['password_ngrams'].apply(lambda x: len(x.intersection(keyboard_ngrams)))
    _df['has_word'] = _df['password_ngrams'].apply(lambda x: len(x.intersection(valid_words)))
    _df.drop(columns=['password_ngrams'], inplace=True)

    if mode == 'train':
        feature_names = [
            'has_digit', 'has_alpha', 'is_digit', 'is_alpha',
            'has_alpha_and_digit', 'has_lower', 'has_upper',
            'has_lower_and_upper', 'len', 'has_keyboard', 'has_word', 'is_alphanumeric'
        ]
        return _df, feature_names

    return _df


def retype(df,
           string_cols: List[str] = [],
           uint_cols: List[str] = [],
           float_cols: List[str] = [],
           bool_cols: List[str] = [],
           exclude_cols: List[str] = []):
    """
    Method to retype columns in df

    :param df: pd.DataFrame
    :param string_cols: List[str]
    :param uint_cols: List[str]
    :param float_cols: List[str]
    :param bool_cols: List[str]
    :param exclude_cols: List[str]
    :return: None
    """

    string_cols = filter(lambda col_name: col_name not in exclude_cols, string_cols)
    uint_cols = filter(lambda col_name: col_name not in exclude_cols, uint_cols)
    float_cols = filter(lambda col_name: col_name not in exclude_cols, float_cols)
    bool_cols = filter(lambda col_name: col_name not in exclude_cols, bool_cols)

    for col in string_cols:
        df[col] = df[col].astype('string')

    for col in uint_cols:
        df[col] = df[col].astype('uint32')

    for col in float_cols:
        df[col] = df[col].astype('float32')

    for col in bool_cols:
        df[col] = df[col].astype('bool')