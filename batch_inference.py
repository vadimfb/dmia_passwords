import argparse
import yaml
import pandas as pd

from model_pipeline.model import PasswordModel

parser = argparse.ArgumentParser()
parser.add_argument('--config_path')

if __name__ == '__main__':

    args = parser.parse_args()
    with open(args.config_path, 'r') as f:
        config = yaml.safe_load(f)

    df = pd.read_csv(config['data_csv'], engine='python')

    model = PasswordModel.load(config['model_path'])
    X = df[['Password']]
    preds = model.predict(X)
    df['Times'] = preds
    df[['Id', 'Times']].to_csv(config['submission_csv'], index=False)
