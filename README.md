# DMIA passwords project

## Firstly:
install pyenv, then install python 3.8.3
```bash
pyenv install 3.8.3
```
then install pipenv, by next command
```bash
pip install --user pipenv
```
or for OSX
```bash
brew install pipenv
```
then clone repo
```bash
git clone https://gitlab.com/vadimfb/dmia_passwords.git
```
and go to the repo
```bash
cd dmia_passwords
```
install requirements
```bash
pipenv install
```
Then download data from https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords
and set right pathes in presets

## Run train pipeline:

```bash
pipenv run python train_model.py --config_path presets/train_config.yaml
```

## Run submit pipeline:

```bash
pipenv run python batch_inference.py --config_path presets/batch_inference_config.yaml
```

## Run app:

```bash
pipenv run python run_app.py
```




