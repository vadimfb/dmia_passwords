import pandas as pd
from flask import Flask, request, render_template
from model_pipeline.model import PasswordModel

model = PasswordModel.load('data/model')


app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        password = request.form["password"]
        if password == '':
            return render_template(
                "index.html",
                prediction='Empty strings are not used that often', password='')
        password_times = model.predict(pd.DataFrame({'Password': [password]}))
        password_times = password_times[0]
        password_times = round(password_times, 2)
        return render_template("index.html", prediction=password_times, password=password)
    else:
        return render_template("index.html")
